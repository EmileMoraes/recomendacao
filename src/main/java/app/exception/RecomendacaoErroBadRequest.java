package app.exception;

public class RecomendacaoErroBadRequest extends Exception{

    private static final long serialVersionUID = -2586209354700102349L;

    public RecomendacaoErroBadRequest(){
        super();
    }

    public RecomendacaoErroBadRequest(String msg){
        super(msg);
    }

    public RecomendacaoErroBadRequest(String msg, Throwable cause){
        super(msg, cause);
    }
}
