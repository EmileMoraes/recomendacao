package app.entrypoint.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class ProdutoDTO {

    @JsonProperty("codigo_produto")
    private String codigoProduto;

    @JsonProperty("nome_produto")
    private String nomeProduto;

    @JsonProperty("sigla_produto")
    private String siglaProduto;

    @JsonProperty("vencimento")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date vencimento;

    @JsonProperty("valor_atual")
    private Integer valorAtual;

    @JsonProperty("valor_resgate")
    private Integer valorResgate;

    @JsonProperty("prazo_dias")
    private Integer prazoDias;
}
