package app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecomendacaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecomendacaoApplication.class, args);
	}

}
/*
TODO:
Mapper da aplicação para fazer o de-para
Construção do dataprovider, controller e as configurações
 */