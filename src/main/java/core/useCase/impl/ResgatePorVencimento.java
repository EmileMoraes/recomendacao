package core.useCase.impl;

import app.entrypoint.dto.RecomendacaoResgateDTO;
import app.entrypoint.dto.RecomendacaoResgateEnumDTO;
import core.RecomendacaoProdutoGateway;
import core.entity.Resgate;
import core.useCase.RecomendacaoResgate;
import org.springframework.beans.factory.annotation.Autowired;

public class ResgatePorVencimento implements RecomendacaoResgate {

    @Autowired
    private RecomendacaoProdutoGateway recomendacaoProdutoGateway;

//Metodo que executa a recomendação quando o tipo for por vencimento
    @Override
    public RecomendacaoResgateDTO regraRecomendacaoResgate(Resgate resgate) {
        System.out.println("Recomendação resgate por Vencimento");
        return RecomendacaoResgateDTO.builder()
                .recomendacaoResgateEnumDTO(RecomendacaoResgateEnumDTO.VENCIMENTO)
                .build();
    }
}
