package app.entrypoint.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import org.springframework.lang.NonNull;

import java.util.Date;
import java.util.List;

@Data
@Builder
public class RecomendacaoResgateDTO {

    @JsonProperty("recomendacao_resgate_tipo")
    private RecomendacaoResgateEnumDTO recomendacaoResgateEnumDTO;

    @JsonProperty("processo_data")
    @NonNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss ")
    private Date processoData;

    @JsonProperty("produtos")
    @NonNull
    private List<ProdutoDTO> produtoDTO;

}