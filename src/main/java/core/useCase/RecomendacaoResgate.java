package core.useCase;

import app.entrypoint.dto.RecomendacaoResgateDTO;
import core.entity.Resgate;

public interface RecomendacaoResgate {
//Contrato que estabelece a regra do resgate
    public RecomendacaoResgateDTO regraRecomendacaoResgate(Resgate resgate);
}
