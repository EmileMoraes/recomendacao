package core.useCase.impl;

import app.entrypoint.dto.RecomendacaoResgateDTO;
import core.RecomendacaoProdutoGateway;
import core.entity.Resgate;
import core.useCase.RecomendacaoResgate;
import org.springframework.beans.factory.annotation.Autowired;

public class ResgatePorBlocklist implements RecomendacaoResgate {

    @Autowired
    private RecomendacaoProdutoGateway recomendacaoProdutoGateway;

    //Metodo que executa a recomendação quando o tipo for por blocklist
    @Override
    public RecomendacaoResgateDTO regraRecomendacaoResgate(Resgate resgate) {
        System.out.println("Recomendação resgate por Blocklist");
        return null;
    }
}
