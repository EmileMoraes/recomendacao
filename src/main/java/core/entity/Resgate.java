package core.entity;

import core.useCase.enums.RecomendacaoResgateEnum;
import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@Builder
public class Resgate {

    private RecomendacaoResgateEnum recomendacaoResgateEnum;
    private Date processoData;
    private List<Produto> produtoList;
}
