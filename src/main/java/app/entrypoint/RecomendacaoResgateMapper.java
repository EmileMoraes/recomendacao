package app.entrypoint;

import app.entrypoint.dto.RecomendacaoResgateDTO;
import core.entity.Resgate;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface RecomendacaoResgateMapper {

    @Mapping(target = ".", source = "resgate")
    RecomendacaoResgateDTO buildRecomendacaoResgateDTO(Resgate resgate);

}
