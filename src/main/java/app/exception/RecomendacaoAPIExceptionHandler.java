package app.exception;

import app.exception.response.ResponseError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RecomendacaoAPIExceptionHandler<T> {


    @ExceptionHandler(value = { RecomendacaoErroBadRequest.class })
    protected ResponseEntity<ResponseError<T>> RecomendacaoErroBadRequest(RecomendacaoErroBadRequest exception) {

        ResponseError<T> response = new ResponseError<>();
        response.addErrorMsgToResponse(exception.getLocalizedMessage());

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }
}
