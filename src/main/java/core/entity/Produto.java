package core.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Produto {

    private String codigoProduto;
    private String nomeProduto;
    private String siglaProduto;
    private Date vencimento;
    private Integer valorAtual;
    private Integer valorResgate;
    private Integer prazoDias;

    public String getSiglaProduto(){
        return siglaProduto.concat("@").concat(codigoProduto);
    }

}
