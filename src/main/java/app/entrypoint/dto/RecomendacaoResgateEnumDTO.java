package app.entrypoint.dto;

public enum RecomendacaoResgateEnumDTO {

    BLOCKLIST("Blocklist"),
    VENCIMENTO("Vencimento");

    private String type;

        RecomendacaoResgateEnumDTO(String type){
            this.type = type;
        }
}
