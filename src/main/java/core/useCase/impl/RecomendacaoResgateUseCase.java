package core.useCase.impl;

import app.entrypoint.dto.RecomendacaoResgateDTO;
import core.entity.Resgate;
import core.useCase.RecomendacaoResgate;
import core.useCase.enums.RecomendacaoResgateEnum;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RecomendacaoResgateUseCase implements RecomendacaoResgate {

    //TODO complementar essa classe

    private final ResgatePorVencimento resgatePorVencimento;
    private final ResgatePorBlocklist resgatePorBlocklist;

    @Override
    public RecomendacaoResgateDTO regraRecomendacaoResgate(Resgate resgate) {
        if(resgate.getRecomendacaoResgateEnum().equals(RecomendacaoResgateEnum.VENCIMENTO)){
            resgatePorVencimento.regraRecomendacaoResgate(resgate);
        }

        if(resgate.getRecomendacaoResgateEnum().equals(RecomendacaoResgateEnum.BLOCKLIST)){
            resgatePorBlocklist.regraRecomendacaoResgate(resgate);
        }
        else {
            resgatePorBlocklist.regraRecomendacaoResgate(resgate);
            resgatePorVencimento.regraRecomendacaoResgate(resgate);
        }

        //TODO Fazer esse return
        return null;
    }
}
